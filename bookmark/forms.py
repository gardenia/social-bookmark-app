import re
from django import forms
from django.contrib.auth.forms import UserCreationForm as AuthUserCreationForm
from django.contrib.auth.models import User
from .models import Bookmark, Tag
from django.forms.utils import ErrorList


class CleanTagsMixin:
    def clean_tags(self):
        tags = re.sub(r'\s+', ' ', self.cleaned_data['tags'])
        tags = tags.strip()
        tags = re.sub(r'[^a-z-_\s]', '', tags)
        tags_list = tags.split(' ')

        if not tags_list:
            return []
        result = []
        for tag in tags_list:
            if tag != "":
                tag_obj, _ = Tag.objects.get_or_create(
                    defaults={'title': tag},
                    title__iexact=tag)
                result.append(tag_obj)
        return result


class BookmarkCreateForm(forms.ModelForm, CleanTagsMixin):
    class Meta:
        model = Bookmark
        fields = ['title', 'comment', 'state', 'tags']

    url = forms.URLField()
    tags = forms.CharField(required=False, max_length=500)
    state = forms.HiddenInput()

    def __init__(self, user, *args, **kwargs):
        super(BookmarkCreateForm, self).__init__(*args, **kwargs)
        self.user = user


class BookmarkEditForm(forms.ModelForm, CleanTagsMixin):
    class Meta:
        model = Bookmark
        fields = ['title', 'comment', 'state', 'tags']

    url = forms.CharField(widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    tags = forms.CharField(widget=forms.Textarea(), required=False)

    def __init__(self, user, *args, **kwargs):
        super(BookmarkEditForm, self).__init__(*args, **kwargs)
        self.initial['url'] = self.instance.link.url
        if self.instance.tags:
            self.initial['tags'] = self.instance.get_str_tags()
        self.user = user

    def clean_url(self):
        return self.initial['url']


class UserCreationForm(AuthUserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def clean_username(self):
        username = self.cleaned_data.get("username")

        exists = User.objects.filter(username=username).exists()

        if exists:
            errors = self._errors.setdefault("username", ErrorList())
            errors.append("This username is in use, choose another one.")
            return ""
        else:
            return username
