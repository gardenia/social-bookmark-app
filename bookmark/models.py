from django.utils import timezone
from django.db import models
from enumfields import EnumField
from django.contrib.auth.models import User
from .enums import BookmarkState, RelationshipState


class Bookmark(models.Model):
    link = models.ForeignKey('Link')
    title = models.CharField(blank=True, max_length=100)
    comment = models.TextField(blank=True)
    state = EnumField(BookmarkState, default=BookmarkState.USER_ONLY)
    user = models.ForeignKey(User)
    date_added = models.DateTimeField(default=timezone.now)
    tags = models.ManyToManyField('Tag', related_name='bookmarks', blank=True)

    def get_tags(self):
        return self.tags.all()

    def is_public(self):
        return self.state == BookmarkState.PUBLIC

    def is_followers(self):
        return self.state == BookmarkState.FOLLOWERS_ONLY

    def is_private(self):
        return self.state == BookmarkState.USER_ONLY

    def get_liked_users(self):
        return User.objects.filter(pk__in=self.like_set.values_list('user', flat=True))

    def get_str_tags(self):
        return ", ".join([tag.title for tag in self.tags.all()])


class Link(models.Model):
    url = models.URLField(db_index=True, unique=True)

    def __str__(self):
        return self.url


class FollowingRelationship(models.Model):
    user = models.ForeignKey(User, related_name='following')
    following = models.ForeignKey(User, related_name='followed_by')
    state = EnumField(RelationshipState, default=RelationshipState.FOLLOW)


def get_followers(self):
    return User.objects.filter(id__in=(FollowingRelationship.objects.values_list('user_id', flat=True).filter(following=self)))


def get_following(self):
    return User.objects.filter(id__in=(FollowingRelationship.objects.values_list('following_id', flat=True).filter(user=self)))

User.add_to_class("get_followers", get_followers)
User.add_to_class("get_following", get_following)


class Tag(models.Model):
    title = models.CharField(max_length=50, db_index=True, unique=True)
    date_added = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

    def get_bookmarks(self):
        return self.bookmarks.all()


class Like (models.Model):
    user = models.ForeignKey(User)
    bookmark = models.ForeignKey(Bookmark)
    date_liked = models.DateTimeField(default=timezone.now)