from django.contrib import admin
from .models import *


class BookmarkAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'user_id', 'comment', 'state', '_link_id', '_tags')
    list_filter = ('state', 'link')

    def _link_id(self, bookmark):
        return bookmark.link.id

    def _tags(self, bookmark):
        return ','.join([tag.title for tag in bookmark.tags.all()])

admin.site.register(Bookmark, BookmarkAdmin)

admin.site.register(Link)


class TagsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'date_added')
admin.site.register(Tag, TagsAdmin)


class FollowRelationshipAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', '_user_id', 'following_id', '_following_id', 'state')

    def _user_id(self, instance):
        return instance.user_id

    def _following_id(self, instance):
        return instance.following_id

admin.site.register(FollowingRelationship, FollowRelationshipAdmin)


class LikeAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'bookmark_id', 'date_liked')

    def bookmark_id(self, like):
        return like.bookmark.id

admin.site.register(Like, LikeAdmin)