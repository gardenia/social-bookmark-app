from django.conf.urls import url
from django.views.generic import TemplateView, RedirectView
from .views import *
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^register/$', UserCreate.as_view(), name='user-create'),
    url(r'^$', RedirectView.as_view(url='home/')),
    url(r'^login/$', auth_views.login, {'template_name': 'index.html', 'redirect_authenticated_user': True, 'extra_context': {'view':'login'}}, name='user-login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='user-logout'),
    url(r'^accounts/profile/(?P<pk>[0-9]+)/$', UserProfile.as_view(), name='user-profile'),
    url(r'^accounts/profile/$', UserProfile.as_view(), name='user-profile'),

    url(r'^home/$', HomeStream.as_view(), name='home-page'),

    url(r'^bookmark/add/$', BookmarkCreate.as_view(), name="bookmark-add"),
    url(r'^bookmark/(?P<pk>[0-9]+)/edit/$', BookmarkEdit.as_view(), name="bookmark-edit"),
    url(r'^bookmark/(?P<pk>[0-9]+)/delete/$', BookmarkDelete.as_view(), name="bookmark-delete"),

    url(r'^bookmark/share/$', BookmarkShare.as_view(), name="bookmark-share"),
    url(r'^bookmark/like/(?P<return_url>[0-9a-z-/]+)/$', BookmarkLike.as_view(), name="bookmark-like"),


    url(r'^bookmark/(?P<tag>[a-z_-]+)/$', BookmarkSearch.as_view(), name='bookmark-search'),
    url(r'^bookmark/$', BookmarkSearch.as_view(), name='bookmark-search'),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='landing-page')


]