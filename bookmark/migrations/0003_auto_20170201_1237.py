# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-01 11:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookmark', '0002_auto_20170131_1530'),
    ]

    operations = [
        migrations.RenameField(
            model_name='followingrelationship',
            old_name='following',
            new_name='followed',
        ),
    ]
