# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-07 14:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookmark', '0005_auto_20170207_1246'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookmark',
            name='tags',
        ),
        migrations.DeleteModel(
            name='Tag',
        ),
    ]
