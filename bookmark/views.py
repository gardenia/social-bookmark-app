from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required

from django.db.models import Q, Count
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from .models import *
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.base import View
from django.views.generic import ListView, DetailView
from .forms import BookmarkCreateForm, BookmarkEditForm, UserCreationForm
from django.urls import reverse_lazy


@method_decorator(login_required, name='dispatch')
class BookmarkCreate(CreateView):
    model = Bookmark
    form_class = BookmarkCreateForm
    template_name = 'bookmark_add.html'
    success_url = reverse_lazy('user-profile')

    def form_valid(self, form):
        form.instance.link, _ = Link.objects.get_or_create(url=form.cleaned_data['url'])
        form.instance.user = self.request.user
        return super(BookmarkCreate, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(BookmarkCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


@method_decorator(login_required, name='dispatch')
class BookmarkShare(CreateView):
    model = Bookmark
    form_class = BookmarkCreateForm
    template_name = 'bookmark_add.html'
    success_url = reverse_lazy('user-profile')

    def get_initial(self):
        initial = super(BookmarkShare, self).get_initial()
        initial['url'] = self.request.GET['url']
        self.link = Link.objects.get(url=initial['url'])
        return initial

    def get_form_kwargs(self):
        kwargs = super(BookmarkShare, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        form.instance.link = self.link
        form.instance.user = self.request.user
        return super(BookmarkShare, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class BookmarkEdit(UpdateView):
    model = Bookmark
    form_class = BookmarkEditForm
    template_name = 'bookmark_edit.html'
    success_url = reverse_lazy('user-profile')

    def get_form_kwargs(self):
        kwargs = super(BookmarkEdit, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


@method_decorator(login_required, name='dispatch')
class BookmarkDelete(DeleteView):
    model = Bookmark
    success_url = reverse_lazy('user-profile')


# @method_decorator(login_required, name='dispatch')
# class UserBookmarkList(ListView):
#     template_name = 'bookmark_list.html'
#
#     def get_queryset(self):
#         self.user = get_object_or_404(User, id=self.kwargs['pk'])
#         if self.user == self.request.user:
#             return Bookmark.objects.filter(user=self.user)
#         elif self.request.user.id in self.user.followed_by.all().values_list('user_id', flat=True):
#             return Bookmark.objects.filter(user=self.user).exclude(state=BookmarkState.USER_ONLY)
#         else:
#             return Bookmark.objects.filter(user=self.user, state=BookmarkState.PUBLIC)
#
#     def get_context_data(self, **kwargs):
#         context = super(UserBookmarkList, self).get_context_data(**kwargs)
#         # context['tags'] = Tag.objects.all().annotate(bookmarks_count=Count('bookmarks')).order_by('-bookmarks_count')
#         context['author'] = self.user
#         return context


# @method_decorator(login_required, name='dispatch')
# class BookmarkDetail(DetailView):
#     model = Bookmark
#     # template_name = 'bookmark_detail.html'
#     template_name = 'bookmark.html'


@method_decorator(login_required, name='dispatch')
class BookmarkLike(View):
    def post(self, request, *args, **kwargs):
        if request.POST['action'] == 'like':
            Like.objects.create(user=request.user, bookmark_id=int(request.POST['bookmark_id']))
        elif request.POST['action'] == 'unlike':
            Like.objects.filter(user=request.user, bookmark_id=int(request.POST['bookmark_id'])).delete()
        return HttpResponseRedirect(kwargs['return_url'])


class UserCreate(CreateView):
    model = User
    form_class = UserCreationForm
    template_name = 'index.html'
    success_url = reverse_lazy('user-profile')

    def form_valid(self, form):
        user = super(UserCreate, self).form_valid(form)
        user.email = form.cleaned_data['email']

        newuser = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
        login(self.request, newuser)
        return user

    def get_context_data(self, **kwargs):
        context = super(UserCreate, self).get_context_data(**kwargs)
        if not context:
            context = {}
        context['view'] = 'register'
        return context


@method_decorator(login_required, name='dispatch')
class UserProfile(DetailView):
    template_name = 'profile.html'
    context_object_name = 'viewed_user'
    queryset = User.objects.all()

    def get_object(self, queryset=None):
        try:
            obj = super(UserProfile, self).get_object(queryset)
        except AttributeError:
            obj = self.queryset.filter(pk=self.request.user.id).get()
        return obj

    def post(self, request, *args, **kwargs):
        user = self.get_object()

        if request.POST['action'] == 'follow':
            FollowingRelationship.objects.create(user=request.user, following=user)
        elif request.POST['action'] == 'unfollow':
            FollowingRelationship.objects.filter(user=request.user, following=user).delete()

        return HttpResponseRedirect(reverse_lazy('user-profile', kwargs={'pk': user.pk}))

    def get_context_data(self, **kwargs):
        context = super(UserProfile, self).get_context_data(**kwargs)
        context['followers'] = self.object.get_followers()
        if self.request.user in context['followers']:
            bookmarks = Bookmark.objects.filter(user=self.object).exclude(state=BookmarkState.USER_ONLY)
        elif self.object == self.request.user:
            bookmarks = Bookmark.objects.filter(user=self.object)
        else:
            bookmarks = Bookmark.objects.filter(user=self.object, state=BookmarkState.PUBLIC)
        context['bookmarks'] = bookmarks
        return context


@method_decorator(login_required, name='dispatch')
class HomeStream(ListView):
    template_name = 'home.html'

    def get_queryset(self):
        return Bookmark.objects.filter(user__in=(self.request.user.get_following())).exclude(state=BookmarkState.USER_ONLY)


@method_decorator(login_required, name='dispatch')
class BookmarkSearch(ListView):
    template_name = 'bookmark_list.html'
    searchword = ''

    def get_queryset(self):
        q = Q()
        user = self.request.user
        if 'searchText' in self.request.GET:
            text = self.request.GET['searchText']
            self.searchword = text
            q = Q()
            for keyword in text.strip().split(' '):
                q &= Q(Q(title__icontains=keyword) | Q(comment__icontains=keyword) | Q(link__url__icontains=keyword))
            # q &= (Q(user__in=user.get_following()) & Q(state__exact=BookmarkState.FOLLOWERS_ONLY)) | \
            #      Q(user__exact=user) | Q(state__exact=BookmarkState.PUBLIC)
            # qs = Bookmark.objects.filter(q)
            # return qs
        elif 'tag' in self.kwargs:
            tag = self.kwargs['tag']
            self.searchword = tag
            q = Q(tags__title__iexact=tag)

        q &= (Q(user__in=user.get_following()) & Q(state__exact=BookmarkState.FOLLOWERS_ONLY)) | \
                 Q(user__exact=user) | Q(state__exact=BookmarkState.PUBLIC)
        qs = Bookmark.objects.filter(q)
        return qs

    def get_context_data(self, **kwargs):
        context = super(BookmarkSearch, self).get_context_data(**kwargs)
        context['searchword'] = self.searchword
        return context
