from enumfields import Enum


class BookmarkState(Enum):
    PUBLIC = 0
    FOLLOWERS_ONLY = 1
    USER_ONLY = 2


class RelationshipState(Enum):
    FOLLOW = 0
    BLOCKED = 1
