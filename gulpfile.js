var gulp = require('gulp'),
    watch = require('gulp-watch'),
    cleancss = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    coffee = require('gulp-coffee'),
    uglify = require('gulp-uglify'),
    compass = require('gulp-compass'),
    plumber = require('gulp-plumber'),
    concat = require('gulp-concat'),
    run = require('gulp-run'),
    os_path = require('path');

function path(p) {
    return os_path.join.apply(os_path, p.split('/'));
}

var sass_path = path('static_files/sass');
var sass_files = path('static_files/sass/*.sass');
var css_path = path('static_files/css');
var css_files = path('static_files/css/*.css');
var js_path = path('static_files/js');
var coffee_files = path('static_files/js/*.coffee');
var js_files = path('static_files/js/*.js');

function swallowError (error) {
    console.log(error.toString());
    this.emit('end');
}

gulp.task('compass', function () {
    return gulp.src(sass_files)
        .pipe(plumber({
            errorHandler: swallowError
        }))
        .pipe(compass({
            css: css_path,
            sass: sass_path,
            image: path('static_files/images'),
            comments: true,
            style: 'expanded'
        }))
        .pipe(gulp.dest(css_path));
});

gulp.task('coffee', function () {
    gulp.src(coffee_files)
        .pipe(plumber({
            errorHandler: swallowError
        }))
        .pipe(coffee({bare: true}))
        .pipe(gulp.dest(js_path))
});

gulp.task('watch', function () {
    gulp.watch(sass_files, ['compass']);
    gulp.watch(coffee_files, ['coffee']);
});

gulp.task('default', ['compass', 'coffee', 'watch']);

gulp.task('build-css', function () {
    return gulp.src(css_files)
        .pipe(plumber())
        .pipe(cleancss())
        .pipe(concat('all.css'))
        .pipe(gulp.dest(path('build/css')));
});

gulp.task('build-js', function () {
    return gulp.src(js_files)
        .pipe(plumber())
        .pipe(uglify())
        .pipe(concat('all.js'))
        .pipe(gulp.dest(path('build/js')));
});

gulp.task('build', ['compass', 'coffee', 'build-css', 'build-js']);
