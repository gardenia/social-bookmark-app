from . import settings
from django.db.models import Count, Q
from bookmark.models import Tag, BookmarkState, Bookmark


def default(request):
    context = {}
    if request.user.is_authenticated:
        # context['TOPTAGS'] = Tag.objects.all().annotate(bookmarks_count=Count('bookmarks')).order_by('-bookmarks_count')
        user = request.user
        q = (Q(user__in=user.get_following()) & Q(state__exact=BookmarkState.FOLLOWERS_ONLY)) | \
                 Q(user__exact=user) | Q(state__exact=BookmarkState.PUBLIC)
        bookmarks = Bookmark.objects.filter(q).values_list('id', flat=True)

        Tags = Tag.objects.filter(bookmarks__id__in=bookmarks).annotate(bookmarks_count=Count('bookmarks')).order_by('-bookmarks_count')
        context['TOPTAGS'] = Tags
    return context