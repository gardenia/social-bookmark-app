This is a simple project that I did as an exercise, it’s a small app that allows users to save their links as bookmarks, it also gives statistics about the most used tags in public bookmarks, using a very simple and basic interface.
The application specifications:
-	Users share links.
-	Links can be shared by multiple users.
-	A link shared is called a bookmark.
-	A bookmark can have a title, and some text as a comment on it.
-	Bookmarks can have tags.
-	A bookmark can either be public, followers-only , user-only.
-	Users can follow users.
-	Users see the bookmarks of users they follow on the homepage.
-	Users can like bookmarks.
-	Users can comment on bookmarks.
-	A user can edit/delete own bookmarks.
-	Users can search bookmarks, the app searches in title, description and URL.
